<?php
//Корневая дирректория
define('ROOT', dirname(__FILE__));
//Подключение к ДБ
require_once (ROOT . '/components/Db.php');

$db = Db::getConnection();
//
if (isset($db)) {
    echo "База подключена";
} else
    echo "Не удалось подключиться к базе";
//

class FormQuery
{


    //Функция генератора выпадающего списка
    public function selectorBuilder($db, $table, $id, $name)
    {
        $result = $db->query('SELECT ' . $id . ', ' . $name . ' FROM ' . $table . ' ');

        //Формирование селектора
        ?>
        <p><select id="<?= $table ?>">
                <?php
                while ($row = $result->fetch(PDO::FETCH_LAZY)) {
                    ?>
                    <option value="<?= $row[$id] ?>" name="<?= $table ?>"><?= $row[$name] ?></option>
                    <?php
                }
                ?>
            </select></p>
        <?php
    }

    //Формирование таблицы
    public function tableBuilder($db){
        $result = $db->query('SELECT *  FROM  records');
        ?>
        <input type="text" id="myInput" onkeyup="tableFilter()" placeholder="Поиск по имени..">
        <table border="1" id="myTable">
            <tr>
                <th>№</th>
                <th>Курьер</th>
                <th>Пункт назначения</th>
                <th>Дата начала</th>
                <th>Дата окончания</th>
                <th>Время начала</th>
                <th>Время окончания</th>
            </tr>
                <?php
                foreach ( $result as $row) {
                    $id_courier = $row['id_courier'];
                    $tmp1 = $db->prepare("SELECT name FROM couriers WHERE id= '$id_courier' ");
                    $tmp1->execute(array($row['id_courier']));
                    $courierName = $tmp1->fetchColumn();

                    $id_destination = $row['id_destination'];
                    $tmp2 = $db->prepare("SELECT destination FROM destinations WHERE id= '$id_destination' ");
                    $tmp2->execute(array($row['id_destination']));
                    $destination = $tmp2->fetchColumn();
                    ?>
                    <tr>
                        <td><?= $row['id'] ?></td>
                        <td><?= $courierName ?></td>
                        <td><?= $destination ?></td>
                        <td><?= $row['start_date'] ?></td>
                        <td><?= $row['end_date'] ?></td>
                        <td><?= $row['start_time'] ?></td>
                        <td><?= $row['end_time'] ?></td>
                    </tr>
                    <?php
                }
                ?>
        </table>

<!--       Фильтр по курьерам          -->
        <script>
            function tableFilter() {
                var input, filter, table, tr, td, i, txtValue;
                input = document.getElementById("myInput");
                filter = input.value.toUpperCase();
                table = document.getElementById("myTable");
                tr = table.getElementsByTagName("tr");


                for (i = 0; i < tr.length; i++) {
                    //По какому столбцу фильтровать
                    td = tr[i].getElementsByTagName("td")[1];
                    if (td) {
                        txtValue = td.textContent || td.innerText;
                        if (txtValue.toUpperCase().indexOf(filter) > -1) {
                            tr[i].style.display = "";
                        } else {
                            tr[i].style.display = "none";
                        }
                    }
                }
            }
        </script>
        <?php

    }

}




?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form action="send.php" method="post" id="mainForm">
<?php

    $selector = new FormQuery;
    //Список курьеров
    ?><p>Выберите курьера:</p><?php
    $selector->selectorBuilder($db, 'couriers', 'id', 'name');
    //Список пунктов назначения
    ?><p>Выберите пункт назначения:</p><?php
    $selector->selectorBuilder($db, 'destinations', 'id', 'destination');
    ?>


    <p>Дата и время начала заказа:</p>
    <p><input id="start_date" type="date"  required="required"><input id="start_time" type="time" style="margin-left:10px;" required="required"></p>
    <p>Дата и время окончания заказа:</p>
    <p><input id="end_date" type="date"><input id="end_time"type="time" style="margin-left:10px;" required="required"></p>


    <p><input type="submit" value="Отправить"></p>
    <?php
    $selector->tableBuilder($db);
    ?>
</form>
<?php

//Добавление данных в таблицу
//$sql = "INSERT INTO Students (name, lastname, email) VALUES ('Thom', 'Vial', 'thom.v@some.com')";


?>

<script src="js/common.js"></script>
</body>
</html>

