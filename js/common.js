//Преобразование даты из input'a date
function getDate(inp_name) {
    var name_date  = inp_name;
    var Obj = document.getElementById(name_date);
    var date = new Date(Obj.value);
    var day = date.getDate();
    var mn = date.getMonth()+1;
    var yy = date.getFullYear();
    var finalDate = yy+'-'+mn+'-'+day;
    return finalDate;
}

    /** Чистый JS */
    var form = document.querySelector('#mainForm');

    //Получение данных из input's и отправка их на обработку в send.php
    form.addEventListener('submit', function (evt) {
        evt.preventDefault();

        //Получаем значения
        var selObj1 = document.getElementById("couriers");
        var selObj2 = document.getElementById("destinations");
        var selStartDate= getDate("start_date");
        var selEndDate= getDate("end_date");
        var selStartTime = document.getElementById("start_time").value;
        var selEndTime = document.getElementById("end_time").value;

        //Проверка корректности дат (дата окончания заказа не может быть старше начала)
        if  ( (new Date(selStartDate.replace(/(\d+)-(\d+)-(\d+)/, '$2/$3/$1'))) > (new Date(selEndDate.replace(/(\d+)-(\d+)-(\d+)/, '$2/$3/$1'))) )
            alert('Дата конца выполнения не может быть раньше даты начала заказа.');

    else{

        //Формируем окончательные данные для отправки
        var formData = {
            courier : selObj1.options[selObj1.selectedIndex].value,
            destination : selObj2.options[selObj2.selectedIndex].value,
            start_date : selStartDate,
            end_date: selEndDate,
            start_time: selStartTime,
            end_time: selEndTime
        };

        var request = new XMLHttpRequest();

        request.addEventListener('load', function () {
            // В этой части кода можно обрабатывать ответ от сервера
            // console.log(request.response);
            alert('Ваша заявка успешно отправлена!');

        });

        //Отправляем данные в send.php
        request.open('POST', './send.php', true);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        request.send('courier=' + encodeURIComponent(formData.courier)
            + '&destination=' + encodeURIComponent(formData.destination)
            + '&start_date=' + encodeURIComponent(formData.start_date)
            + '&end_date=' + encodeURIComponent(formData.end_date)
            + '&start_time=' + encodeURIComponent(formData.start_time)
            + '&end_time=' + encodeURIComponent(formData.end_time)
        );
        }





    });







