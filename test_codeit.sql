-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 06 2019 г., 20:07
-- Версия сервера: 10.3.9-MariaDB
-- Версия PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test_codeit`
--

-- --------------------------------------------------------

--
-- Структура таблицы `couriers`
--

CREATE TABLE `couriers` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `couriers`
--

INSERT INTO `couriers` (`id`, `name`) VALUES
(1, 'Сидоров Д.И.'),
(2, 'Иванов Г.А.'),
(3, 'Фролов В.В.'),
(4, 'Краснобаев А.А.');

-- --------------------------------------------------------

--
-- Структура таблицы `destinations`
--

CREATE TABLE `destinations` (
  `id` int(11) NOT NULL,
  `destination` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `destinations`
--

INSERT INTO `destinations` (`id`, `destination`) VALUES
(1, 'Харьков'),
(2, 'Киев'),
(3, 'Одесса'),
(4, 'Полтава');

-- --------------------------------------------------------

--
-- Структура таблицы `records`
--

CREATE TABLE `records` (
  `id` int(11) NOT NULL,
  `id_courier` int(11) NOT NULL,
  `id_destination` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `records`
--

INSERT INTO `records` (`id`, `id_courier`, `id_destination`, `start_date`, `end_date`, `start_time`, `end_time`) VALUES
(1, 1, 1, '2019-06-03', '2019-06-22', '10:11:00', '23:08:00'),
(2, 1, 1, '2019-06-28', '2019-06-03', '10:11:00', '23:08:00'),
(3, 1, 1, '2019-06-05', '2019-06-30', '10:11:00', '23:08:00'),
(4, 1, 1, '2019-06-03', '2019-06-20', '10:11:00', '23:08:00'),
(5, 1, 1, '2019-06-28', '2019-06-29', '10:11:00', '23:08:00'),
(6, 1, 1, '1970-01-01', '1970-01-01', '10:11:00', '23:08:00'),
(7, 3, 4, '2019-06-24', '2019-06-30', '10:11:00', '23:08:00'),
(8, 1, 1, '2019-06-18', '2019-06-20', '23:01:00', '16:42:00'),
(9, 2, 4, '2019-06-10', '2019-06-21', '15:02:00', '16:07:00'),
(10, 2, 4, '2019-06-04', '2019-06-22', '05:52:00', '23:05:00'),
(11, 4, 4, '2019-06-01', '2019-06-05', '23:52:00', '05:25:00'),
(12, 4, 4, '2019-06-03', '2019-06-15', '23:05:00', '23:05:00'),
(13, 3, 2, '2019-06-05', '2019-06-13', '23:05:00', '12:04:00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `couriers`
--
ALTER TABLE `couriers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `destinations`
--
ALTER TABLE `destinations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `records`
--
ALTER TABLE `records`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `couriers`
--
ALTER TABLE `couriers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `destinations`
--
ALTER TABLE `destinations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `records`
--
ALTER TABLE `records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
